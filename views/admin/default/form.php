<?php
/* @var $form CActiveForm */
/* @var $model Page */

?>



<div class="form">
<?php $form=$this->beginWidget('ActiveForm', array(
		'id'=>'mainForm',
		'htmlOptions' => array('enctype' => 'multipart/form-data',),
)); 


$this->widget('TabView',array(
		'id' => 'sliderTabs',
		'cssFile' => false,
		'tabs'=>array(
				'general' =>  array(
									'title' => Yii::t('form', 'General'),
									'view' => '_general'
								),
				'slides' =>  array(
						'title' => Yii::t('SliderModule.main', 'Slides'),
						'view' => '_slides'
				),
		),

		'viewData' => CMap::mergeArray(array('form' => $form), $_data_),
		'htmlOptions' => array('class' => 'nav-tabs'),
));
?>

	<?php $this->endWidget(); ?>
</div>