<?php 
$form->fieldGroup('textField', $model, 'name');
$form->fieldGroup('aliasMaker', $model, 'anchor', array ( 'referenceAttribute' => 'name') ); 

if(count(Yii::app()->languageManager->langs)>1)
	$form->fieldGroup('dropDownList', $model, 'lang', array('data' => Lang::getListData()));

$form->fieldGroup('textField', $model, 'animduration');
$form->fieldGroup('textField', $model, 'animpause');
?>
