<?php if($model->isNewRecord): ?>
	<div class="alert alert-warning"><?=Yii::t('SliderModule.main', 'You must save this Slider before adding slides.')?></div>
<?php else:?>
<div style="padding: 20px;">
		<p>
		<?= CHtml::htmlButton('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('SliderModule.main', 'Add Slide'), array('name' => 'add-slide', 'id' => 'add-slide', 'class' => 'btn btn-success'));?>
		</p>
		<?php 

		
		Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish(Yii::getPathOfAlias('modules.slider.assets')). '/css/admin.css' );
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		Yii::app()->clientScript->registerScript('slides', "
							
			$('#slides_container').sortable({
				handle: '.glyphicon-move',
				start: function(e, ui ){
				    ui.placeholder.height(ui.helper.height());
					ui.placeholder.width(ui.helper.width());
				},
				tolerance: 'pointer',
			});
    		$('#slides_container').disableSelection();
			
			$('#slides_container').on('click', '.glyphicon-trash', function(){
				$(this).parents('.slide-container').fadeOut('slow', function(){ $(this).remove(); });
			});
				
				
			$('#add-slide').on('click', function(){
					$(this).parents('form:first').attr('action', '".Yii::app()->request->requestUri."#slides').append('<input type=\"hidden\" name=\"add-slide\" />').submit();
			});
				
			");
		
		?>
		
		<div id="slides_container">
		<?php foreach($slides as $index => $slide): ?>

			<div class="slide-container">
			
				<div class="panel panel-primary">

				
				<div class="panel-heading">&nbsp; 
					<span class="glyphicon glyphicon-move pull-right"></span>
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-trash"></span></button>
					</div>
					</div>
				<div class="panel-body">
						<div class="form-group">
							<?php
								$this->widget('MediaSelector', array('model' => $slide, 'attribute' => '['.$index.']image', 'imagePreset' => 'slider-admin'));
							?>
							</div>
							<?=$form->fieldGroup('textarea', $slide, '['.$index.']legend');?>
							<?php
								$form->fieldGroup('pageSelector', $slide, '['.$index.']link'); 
							?>
							
							<?php $form->fieldGroup('switcher', $slide, '['.$index.']active',  array('items' =>  array(
				
									array('value' => 1,
											'label' => Yii::t('app', 'Enabled'),
											'color' => 'btn-success'),
							
									array('value' => 0,
											'label' => Yii::t('app', 'Disabled'),
											'color' => 'btn-danger'),
							
								)));?>
					</div>	
				</div>
				
			</div>
		<?php endforeach;?>
		</div>

</div>
<?php endif;?>
