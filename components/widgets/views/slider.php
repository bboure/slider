<?php
Yii::app()->clientScript->registerScript('sliderwidget','
		
			$("#slider").bjqs({
		        "height" : 360,
		        "width" : 900,
		        "responsive" : true,
				"animduration" : '.$slider->animduration.',
				"animspeed" : '.$slider->animpause.',
				"showcontrols": false,
				"centermarkers" : false,
		    });
		
		');
?>

<div id="top">
			<div id="slider-container">
				<div id="slider">
					<ul class="bjqs">
					<?php foreach($slider->slides as $slide): ?> 
						<li>
							<?php $img = CHtml::image(Yii::app()->imageCache->createUrl('slider',$slide->image),$slide->legend,array('title' => $slide->legend)); ?>
							<?php if($slide->link != null):?>
								<?=CHtml::link($img,$slide->link);?>
							<?php else:?>
								<?=$img;?>
							<?php endif;?>
						</li>
					<?php endforeach; ?>
					</ul>
				</div>
				
			</div>
</div>